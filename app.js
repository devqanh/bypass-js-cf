const puppeteer = require('puppeteer-extra')
const express = require('express');
const app = express();
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())


app.get('/', function (req, res) {


    if (req.query.url) {
        var url = req.query.url;
    } else {
        var url = "https://tapmod.net";
       }
    puppeteer.launch({headless: true, args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--no-first-run',
            '--no-zygote',
            '--single-process',
            '--disable-gpu'
        ]}).then(async browser => {
        const page = await browser.newPage()
        //await page.goto(url)
        await page.goto(url, { waitUntil: 'networkidle0' });
        // const name = await page.$eval('#iframe_download', el => el.src)
        // const html = await page.content();
        const html = await page.$eval('body', element => element.innerHTML);
        await browser.close()

        res.send(html);

    })


});


app.listen(7000, function () {
    console.log('Running on port 7000.');
});