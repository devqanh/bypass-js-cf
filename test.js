const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const puppeteer = require("puppeteer-extra");
puppeteer.use(StealthPlugin())
puppeteer.launch({headless: true,args: ['--no-sandbox']}).then(async browser => {
    const page = await browser.newPage()
    await page.goto('https://moddroid.co/instagram.html')

    // const name = await page.$eval('#iframe_download', el => el.src)
    const html = await page.content();
    await browser.close()

    console.log(html);

})